#!/bin/bash
#SBATCH -p node
#SBATCH -N 1
#SBATCH -n 16
#SBATCH -t 24:00:00
#SBATCH -J atacseq

snakemake -j 16 --rerun-incomplete  atacseq_all
