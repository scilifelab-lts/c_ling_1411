#! /bin/bash
#
# Copyright (C) 2015 by Per Unneberg
# 

# Use wabi modules
module use /proj/b2013006/sw/modules
module load miniconda3
source activate snakemakelib-devel

# module load snakemake/3.4.3b
module load bowtie2
module load cutadapt
module load bamtools
