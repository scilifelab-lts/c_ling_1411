# Source code for C_Ling_1411

This repository contains source code for WABI project
C_Ling_1411.

## Snakefiles

### Snakefile_main

Snakefile for main analysis. To run the main analysis, issue

    snakemake -s Snakefile_main atacseq_all

### Snakefile_peaks

Snakefile for peak analysis. To run the peak analysis, issue

    snakemake -s Snakefile_peaks ling_annotate_and_filter_all_samples

Includes rules that are not yet part of the main atacseq_all pipeline.
The rules are located at
/proj/b2013006/sw/apps/miniconda3/envs/py3.4/lib/python3.4/site-packages/snakemake_rules
(see config file for more information):

-   **homer.rules:** rules for running homer programs, such as `annotatePeaks.pl`

In addition, it loads a Snakefile in the `rules` directory,
`peaks.rules` (all \*.rules are snakefiles). The rule
`ling_annotate_and_filter_all_samples` is defined here.

### Project specific rules

-   **ling.rules:** defines rules for linking raw sequence data files to
    analysis directory, along with rules to generate the metadata
    files. Should live in directory `rules` for consistency.
-   **rules/peaks.rules:** peak-calling and annotation rules

### hg38.rules

There are several locations for reference data at uppmax. Here we use
the cloudbiolinux setup (<https://github.com/chapmanb/cloudbiolinux>);
root directory `/pica/data/uppnex/reference/biodata/genomes/`.
However, at the time of analysis, hg38 was not yet included.
Therefore, this rules file defines rules to download hg38 and install
it in `/pica/data/uppnex/reference/biodata/genomes/Hsapiens/hg38`; see
the README.

## Configuration files

-   **smlconf.C_Ling_15_02.yaml:** sets program parameters and options for
    main pipeline (rule `atacseq_all`)
-   **smlconf.peak_filtering.yaml:** sets parameters for peak calling

## Python modules

The directory `ling` is a python module that is loaded by the
Snakefiles with the following statements:

    # Add ling module
    sys.path.insert(0, "/pica/v12/b2014240/private/wabi/source")
    from ling import *

There are currently two submodules:

-   **ling/ling.py:** defines a function that defines what sample names
    should look like, based on regular expressions. Usually, we use
    the regular sample names as provided by Scilife. However, we need
    to redefine the sample names here (regexp [0-9]+) as they are
    represented by numbers and not the Scilife names "P[0-9]+\_[0-9]+".
-   **ling/peaks.py:** defines function for filtering peaks and combining
    filtered peak output with annotation files

## Other files

### metadata.csv

Contains metadata for samples. Assigns submitted sample ids to Scilife
sample ids.

### sampleinfo.csv

Although snakemakelib can autodetect inputs, it is convenient to write
a sample info file which defines the format of each sample. Here we
use a custom naming convention, which for practical purposes means we
need to, for each sample, define the read group tags SM, DT, PU1, and
PU2, along with tags SAMPLE1, SAMPLE2 (see `ling/ling.py`, function
`ling_sample_organization`). The latter are needed since there is a
one-to-many relationship between the submitted sample ids and Scilife
sample ids. The first lines read

    SM,DT,PU1,PU2,SAMPLE1,SAMPLE2
    168,150716,AC71KMANXX,5,P2273,1009
    174,150716,AC71KMANXX,3,P2273,1006

### Blacklist files

-   **wgEncodeDacMapabilityConsensusExcludable.hg38.bed:** provided by
    Maria Suciu
-   **wgEncodeDukeMapabilityRegionsExcludable.bed:** provided by Agata
    Smielowska (BILS support)

### Cluster configuration file

`cluster.yaml` fine-tunes cluster submission parameters for some rules
that require more resources (e.g. RAM). This file is only intended for
use should one use the `--cluster` parameter:

    snakemake -s Snakefile atacseq_all --cluster-config cluster.yaml --cluster " sbatch -A {cluster.account} -p {cluster.core} -n {cluster.n} -t {cluster.time}"
