# -*- snakemake -*-
from snakemakelib.utils import find_files
# Rule to link data from input directory
SOURCEDIR  = "/pica/v12/b2014240/private/delivery"
DEMUXFILES = [os.path.join(SOURCEDIR, "C.Ling_15_01", "Undetermined_indices", "4_150313_BC6J2CANXX_P1895_1003_1.fastq.gz"),
              os.path.join(SOURCEDIR, "C.Ling_15_01", "Undetermined_indices", "4_150313_BC6J2CANXX_P1895_1003_2.fastq.gz")]
INPUTFILES = {
    "C.Ling_15_02" : find_files("P[0-9]+_[0-9]+_[12].fastq.gz$", path=os.path.join(SOURCEDIR, "C.Ling_15_02"), search=True),
    "C.Ling_15_01" : [x for x in find_files("P[0-9]+_[0-9]+_[12].fastq.gz$", path=os.path.join(SOURCEDIR, "C.Ling_15_01"), search=True) if not "P1895_1003" in x] + DEMUXFILES,
    "C.Ling_14_01" : find_files("P[0-9]+_[0-9]+_[12].fastq.gz$", path=os.path.join(SOURCEDIR, "C.Ling_14_01"), search=True)}

# Rules
rule link_source_data:
    input: **INPUTFILES
    run:
        for delivery in dict(input).keys():
            if not delivery in config['delivery']:
                continue
            for source in input[delivery]:
                link = os.path.join(os.curdir, os.path.relpath(source.replace("old_data_126bp", ""), os.path.join(SOURCEDIR, delivery)))
                dirname = os.path.dirname(link)
                # Special case Undetermined_indices
                if dirname.endswith("Undetermined_indices"):
                    dirname = "P1895_1003/150313_BC6J2CANXX"
                    link = os.path.join(dirname, os.path.basename(link))
                if not os.path.exists(dirname):
                    print ("Making directories {dirname}".format(dirname=dirname))
                    os.makedirs(dirname)
                if not os.path.exists(link):
                    print ("Symlinking: ln -s {source} {link}".format(source=source, link=link))
                    os.symlink(source, link)

rule list_input_files:
    run:
        print (INPUTFILES)

rule demultiplex_P1895_1003_1:
    input: fq = os.path.join(SOURCEDIR, "C.Ling_15_01", "Undetermined_indices", "lane4_Undetermined_L004_R1_001.fastq.gz")
    output: fq = protected(os.path.join(SOURCEDIR, "C.Ling_15_01", "Undetermined_indices", "4_150313_BC6J2CANXX_P1895_1003_1.fastq.gz"))
    params: barcode = "CGTACTAGTATCCTCT"
    shell: "zcat {input.fq} | awk '{{FS=\"\\n\"; if (match($1, \"CGTACTAGTATCCTCT\") > 0) {{print;getline;print;getline;print;getline;print;}} else {{getline;getline;getline;}}}}' | gzip > {output.fq}"

rule demultiplex_P1895_1003_2:
    input: fq = os.path.join(SOURCEDIR, "C.Ling_15_01", "Undetermined_indices", "lane4_Undetermined_L004_R2_001.fastq.gz")
    output: fq = protected(os.path.join(SOURCEDIR, "C.Ling_15_01", "Undetermined_indices", "4_150313_BC6J2CANXX_P1895_1003_2.fastq.gz"))
    params: barcode = "CGTACTAGTATCCTCT"
    shell: "zcat {input.fq} | awk '{{FS=\"\\n\"; if (match($1, \"CGTACTAGTATCCTCT\") > 0) {{print;getline;print;getline;print;getline;print;}} else {{getline;getline;getline;}}}}' | gzip > {output.fq}"
        
rule demultiplex_P1895_1003:
    input: fq = [os.path.join(SOURCEDIR, "C.Ling_15_01", "Undetermined_indices", os.path.basename(x)) for x in find_files("P[0-9]+_[0-9]+_[12].fastq.gz$", path=os.path.join(SOURCEDIR, "C.Ling_15_01"), search=True) if "P1895_1003" in x]


ANALYSIS = os.path.basename(os.path.abspath(os.curdir))
WIGFILES = [x for x in find_files("[0-9A-Za-z_\.]+.bw$", path=os.curdir, search=True)]
MACSFILES = [x for x in find_files("[0-9A-Za-z_\.]+.offset_peaks.*$", path=os.curdir, search=True)]
MACSLOGFILES = [x for x in find_files("[0-9A-Za-z_\.]+.offset.*log$", path=os.curdir, search=True)]
SYNCFILES = ["atacseq_summary.html"]
WEBEXPORT = os.path.join("/proj/b2014240/webexport/results/", ANALYSIS)
rule sync_webexport:
    params: webexport=WEBEXPORT
    input: expand(os.path.join("report", "{result}"), result=SYNCFILES) + WIGFILES + MACSFILES + MACSLOGFILES
    output: expand(os.path.join(WEBEXPORT, "{result}"), result=SYNCFILES) + expand(os.path.join(WEBEXPORT, "{wig}"), wig=WIGFILES)
    shell: "rsync -av {input} {params.webexport}; touch {output}"

# rule bedtools_genome:
#     """Make input file to genomeCoverageBed"""
#     input: config['bio.ngs.settings']['db']['ref'].replace(".fa", ".dict")
#     output: 'hg19.bedtools'
#     shell: "cat {input} | grep SN | sed -e s/SN://g | sed -e s/LN://g | awk '{{printf(\"%s\t%s%s\", $2, $3, RS)}}' > {output}"

import pysam
rule ling_check_insert_sizes:
    input: bam = "{prefix}.bam"
    output: hist = "{prefix}.hist"
    run:
        # Use pysam to modify input
        samfile = pysam.AlignmentFile(input.bam, "rb")
        d = {}
        for s in samfile:
            # Modify s here
            if not s.is_unmapped:
                isize = abs(s.tlen)
                if not isize in d:
                    d[isize] = 0
                else:
                    d[isize] += 1
        with open(output.hist, "w") as fh:
            fh.write(str(sorted(d.items())))


P1895_150716 = find_files("150716_AC71KMANXX_P1895_[0-9]+_[12].fastq.gz$", path=SOURCEDIR, search=True)

# Rule to rename/move samples to conform to metadata
rule samples_from_metadata:
    input: metadata = "metadata.csv"
    run:
        import pandas as pd
        import re
        import os
        
        metadata = pd.read_csv(input.metadata)
        d = {x:[] for x in metadata["Donor Id"]}
        [d[k].append(v) for (k,v) in zip(metadata["Donor Id"], metadata["Scilifielab ID"])]
        for (donor, idlist) in sorted(d.items()):
            print("Working on donor ", donor)
            if not re.match("[0-9]+", donor):
                continue
            for sid in idlist:
                print("    sample", sid)
                runs = [x for x in os.listdir(sid) if re.match("[0-9]+", x)]
                for r in runs:
                    donorrun = os.path.join(donor, r)
                    if not os.path.exists(donorrun):
                        os.makedirs(donorrun)
                    sourcerun = os.path.join(sid, r)
                    sourcefiles = [os.path.join(sourcerun, x) for x in os.listdir(sourcerun)]
                    for src in sourcefiles:
                        dst = os.path.join(donorrun, os.path.basename(src))
                        os.rename (src, dst)
                    fastqfiles = [x for x in P1895_150716 if sid in x]
                    for fq in fastqfiles:
                        run = os.path.basename(os.path.dirname(fq))
                        donorrun = os.path.join(donor, run)
                        if not os.path.exists(donorrun):
                            os.makedirs(donorrun)
                        dst = os.path.join(donorrun, os.path.basename(fq))
                        if not os.path.exists(dst):
                            os.symlink(fq, dst)
                    
rule sample_metadata:
    input: metadata = "metadata.csv"
    output: sample_metadata = "metadata_sample.csv"
    run:
        import pandas as pd
        df = pd.read_csv(input.metadata)
        grouped = df.groupby("Donor Id")
        df2 = grouped.agg({'Msequenced': 'sum',
                           'Gender': lambda x: list(set(x))[0],
                           'Diabetic/Healthy': lambda x: list(set(x))[0]})
        df2.to_csv(output.sample_metadata)
