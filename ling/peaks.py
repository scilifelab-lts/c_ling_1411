'''

Author: Per Unneberg
Created: Tue Dec  8 16:31:45 2015

'''
import pandas as pd
import numpy as np
import pybedtools
import json

__all__ = ['filter_macs2_peaks', 'combine_peaks_and_annotation']

def filter_macs2_peaks(infile, outfile, logfile, **kwargs):
    """Perform simple filtering of MACS2-called peaks. Input file is *.offset_peaks.xls"""
    peaks = pd.read_csv(infile, engine="python", comment="#", skip_blank_lines=True, delimiter="\t")
    stats = kwargs
    stats.update({
        'infile': infile,
        'number_of_raw_peaks': peaks.shape[0],
        'n_pvalue_filtered': sum(peaks["-log10(pvalue)"] < stats["pvalue"]),
        'n_qvalue_filtered': sum(peaks["-log10(qvalue)"] < stats["qvalue"]),
        'n_pileup_max_filtered': sum(peaks["pileup"] > stats["pileup_max"]),
        'n_pileup_min_filtered': sum(peaks["pileup"] < stats["pileup_min"]),
        'n_fold_enrichment_min_filtered': sum(peaks["fold_enrichment"] < stats["fold_enrichment_min"]),
        'n_fold_enrichment_max_filtered': sum(peaks["fold_enrichment"] > stats["fold_enrichment_max"]),
        'n_blacklist_filtered': 0,
        'n_filtered_union': 0,
    })

    # Filter blacklist regions if applicable
    #
    # Filtering on intervals; convert remaining data frame to
    # pybedtools object
    i_blacklist_remove = [False] * stats['number_of_raw_peaks']
    if len(kwargs['blacklist']) > 0:
        peakstring = "\n".join(["{chr} {start} {end} {name} 0 *".format(**dict(x[["chr", "start", "end", "name"]])) for index, x in peaks.iterrows()])
        peaks_bed = pybedtools.BedTool(peakstring, from_string=True)
        for bl in kwargs['blacklist']:
            blacklist = pybedtools.BedTool(bl)
            blacklist_peak_names = [x.name for x in peaks_bed.intersect(blacklist)]
            # NB: I hope pandas does an exact match here
            i_blacklist_remove = i_blacklist_remove | peaks["name"].isin(blacklist_peak_names)
        stats['n_blacklist_filtered'] = sum(i_blacklist_remove)

    # Combine all filters to a boolean index which we use to subset
    # the peaks data frame
    i_remove = ((peaks["-log10(pvalue)"] < stats["pvalue"])
                | (peaks["-log10(qvalue)"] < stats["qvalue"])
                | (peaks["fold_enrichment"] < stats["fold_enrichment_min"])
                | (peaks["fold_enrichment"] > stats["fold_enrichment_max"])
                | (peaks["pileup"] < stats["pileup_min"])
                | (peaks["pileup"] > stats["pileup_max"])
                | i_blacklist_remove)
    stats['n_filtered_union'] = sum(i_remove)

    # Save the peaks, removing the filtered ones
    peaks[~i_remove].to_csv(outfile, index=False, sep="\t")
    # Save the log output
    with open(logfile, "w") as fh:
        fh.write(json.dumps({k:str(v) for k,v in stats.items()}, indent=4))


def combine_peaks_and_annotation(xls, tsv, outfile):
    """Combine a peaks xls file with an annotatePeaks tsv file"""
    peaks = pd.read_csv(xls, engine="python", comment="#", skip_blank_lines=True, sep="\t")
    peaks.set_index(["name"])
    annot = pd.read_table(tsv, header=0)
    annot.columns = ["name"] + list(annot.columns[1:])
    annot.set_index(["name"])
    annotpeaks = pd.merge(peaks, annot)
    annotpeaks.to_csv(outfile)
