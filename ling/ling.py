# Created: Mon Oct 19 16:50:11 2015
# Copyright (C) 2015 by Per Unneberg
#
# Author: Per Unneberg
#
"""
Helper functions for C_Ling_1402
"""

__all__ = ['ling_sample_organization', 'find_ling_merge_inputs']

from os.path import join, dirname
from collections import namedtuple
from snakemakelib.io import IOTarget
from snakemakelib.targets import make_targets


def ling_sample_organization():
    sample_org = namedtuple('sample_organization', 'raw_run_re run_id_re sample_re')
    sample_organization = {
        # Illumina sequence data as delivered by SciLife
        'ling' : {
            'sample_organization' : sample_org(IOTarget(join("{SM,[0-9]+}", "{DT,[0-9]+}_{PU1,[A-Z0-9]+XX}", "{PU2,[0-9]}_{DT}_{PU1}_{SAMPLE1,[A-Z0-9]+}_{SAMPLE2,[0-9]+}")),
                                               IOTarget(join("{SM,[0-9]+}", "{DT,[0-9]+}_{PU1,[A-Z0-9]+XX}", "{PU2,[0-9]}_{DT}_{PU1}_{SAMPLE1,[A-Z0-9]+}_{SAMPLE2,[0-9]+}")),
                                               IOTarget(join("{SM,[0-9]+}", "{SM}"))),
        },
    }
    
    sampleconfig = {'settings': sample_organization['ling']}
    return sampleconfig

# Specific find merge inputs for ling sample organization
#
# FIXME: old use of make_targets; see example in snakemakelib_workflows.atacseq for current use
def find_ling_merge_inputs(wildcards):
    from snakemake.workflow import config
    sources = make_targets(tgt_re = config['settings']['sample_organization'].run_id_re,
                           samples = config['_samples'],
                           target_suffix = config['bio.ngs.qc.picard']['merge_sam']['suffix'])
    #m = config['settings']['sample_organization'].sample_re.parse(wildcards.prefix)
    #sources = [src for src in sources if dirname(src).startswith(m['PATH'])]
    return sources
